include <constants.scad>;

module base_raw(height){
  cylinder(r = center_size, h = height, $fn = def_fn*5);
}

module central_part(width, height, length, thickness){
    width = width - thickness;
  module side_cylinder(length){
    cylinder(d=thickness, h=length, center=true);
  }
  
  module side(length, height){
      translate([height-thickness/2,0,0]) side_cylinder(length);
      translate([thickness/2,0,0]) side_cylinder(length);
      rotate([90,0,90])translate([-thickness/2,-length/2,thickness/2])
        cube([thickness,length,height-thickness]);
  }

    module x(){
        l=sqrt(width*width+width*width);
        rotate([45,0,0])  side(l, height);
        rotate([-45,0,0]) side(l, height);
    }

    rotate([0,-90,0]){
        //Main hull
        translate([0,width/2,0]) side(length, height);
        translate([0,-width/2,0]) side(length, height);


        count = floor((length-thickness/2)/width);
        if (count % 2 == 0){
            for(i = [1 : 1 : count/2]){
                translate([0,0,i*width-width/2]) x();
                translate([0,0,-i*width+width/2]) x();
            }
        } else {
            x();
            for(i = [1 : 1 : count/2]){
                translate([0,0,i*width]) x();
                translate([0,0,-i*width]) x();
            }
        }
    }
}

central_part(30,20,180,3);