include <constants.scad>;
use <arm_holder.scad>;

module top(){
  points = (num_of_arms > 5) ? num_of_arms/2 : num_of_arms;
  
  for(i = [0 : points])
    rotate([0,0,i*2*arm_angle])translate([center_size-holder_radius, 0, holder_height])rotate([180,0,180])arm_holder();
  
  radius = center_size;
  difference(){
    sphere(r = radius, $fn = def_fn*5);
    sphere(r = radius-holder_height, $fn = def_fn*5);
    translate([0,0,-100])cylinder(r = radius, h=100, $fn = def_fn*5);
    translate([0,0,electonic_height])cylinder(r = radius, h=100, $fn = def_fn*5);
    for(i = [0 : points]){
        rotate([0,0,i*2*arm_angle+arm_angle])translate([radius,0,-radius/2])sphere(r=radius+1, $fn = def_fn*5);
    }
  }
  translate([0,0,electonic_height-holder_height])cylinder(r = radius-34, h=holder_height, $fn = def_fn*5);
}

top();
