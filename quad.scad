include <constants.scad>;
use <arm_holder.scad>;
use <base.scad>;
use <arm_v2.scad>;
use <top.scad>;
use <leg.scad>;

//base
base();

//arms
for(i = [0 : arm_angle : 359])
  rotate([180,0,i])translate([center_size+arm_shift-holder_radius/2-2, 0, -arm_height-center_height]) arm();

//top
rotate([0,0,arm_angle])translate([0,0,arm_height+center_height])top();

//legs
for(i = [0 : arm_angle : 359])
  rotate([180,0,i])translate([center_size-holder_radius, 0, 0])leg();

if(num_of_arms > 5)
  for(i = [0 : num_of_arms/2])
    rotate([0,0,i*2*arm_angle])translate([center_size-holder_radius, 0, arm_height+center_height+holder_height])rotate([180,0,180])arm_holder();