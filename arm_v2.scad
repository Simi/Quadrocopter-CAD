include <constants.scad>;
use <modules.scad>;

module arm()
{
  //Mount
  translate([-arm_shift-holder_radius/2+2,0,0]){
    mount();
  }
  
  //Central Part
  translate([0,arm_width/2,motor_height/2])rotate([90,0,0])central_part(motor_height,arm_width,arm_length,side_thickness/1.4);
  
  //Motor Holder
  translate([arm_shift+motor_radius/2+1.4,0,0]){
    rotate([0,0,45]){
      motor_holder();
    }
  }
}

module motor_holder()
{
  difference() {
    //Base
    holder(motor_radius);
    
    //Motor Hole
    translate([0,0,1]){
      hole(motor_hole_radius);
    }
    
    //Screw Holes
    translate([hole_distance1,0,1]){
      hole(motor_screw_radius);
    }
    translate([-hole_distance1,0,1]){
      hole(motor_screw_radius);
    }
    translate([0,hole_distance2,1]){
      hole(motor_screw_radius);
    }
    translate([0,-hole_distance2,1]){
      hole(motor_screw_radius);
    }
  }
}

module mount()
{
  difference() {
    //Base
    holder(holder_radius);
    
    //Mount Holes
    translate([0,-hole_distance2,1]){
      hole(hole_radius);
    }
    
    translate([0,hole_distance2,1]){
      hole(hole_radius);
    }
  }
}

module hole(radius)
{
  cylinder(r = radius, h = motor_height * 2, center = true);
}

module holeh(radius, height)
{
  cylinder(r = radius, h = height, center = true);
}

module holder(radius)
{
  translate([0,0,0])cylinder(r = radius, h = motor_height, center = false);
}

arm();