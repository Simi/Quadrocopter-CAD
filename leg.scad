include <constants.scad>;
use <arm_v2.scad>;
use <modules.scad>;



module leg(){
  shift_down = 4;
  shift_leg = [holder_radius/1.5,0,-shift_down];
  
  difference(){
    union(){
      translate([0,0,-arm_height/2+2.5])mount();
      
      translate(shift_leg) 
        rotate([90,-45,0])
          translate([leg_length/2-3,8,-arm_height/2])
            central_part(arm_width,motor_height,leg_length,side_thickness);
    }
    
    //cut for 3D printing
    translate(shift_leg/1.26)rotate([0,-45,0])translate([-10,-leg_width*2,-leg_height-leg_height/2])cube([leg_length*1.5,leg_width*4,leg_height]);
   translate([-holder_radius,-holder_radius, -holder_radius*2-arm_height/2+2.5]) cube(holder_radius*2);
    
    //cut for end of leg
    translate([leg_length*sin(45)-leg_width*2-5,-leg_width*2,leg_length*sin(45)-leg_height/2*sin(45)-shift_down-2.5])cube([leg_width*4,leg_width*4,leg_width*4]);
    
    
    //Mount Holes
    translate([0,-hole_distance2,1]){
      hole(hole_radius);
    }
    
    translate([0,hole_distance2,1]){
      hole(hole_radius);
    }
    
    //screw holes
    translate([0,-hole_distance2,motor_height*2-arm_height/2+2.5]){
      hole(5);
    }
    
    translate([0,hole_distance2,motor_height*2-arm_height/2+2.5]){
      hole(5);
    }
  }
  
}

leg();