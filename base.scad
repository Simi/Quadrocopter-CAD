include <constants.scad>;
use <arm_v2.scad>;
use <leg.scad>;
use <modules.scad>;
use <arm_holder.scad>;

module mount_holes(){
  translate([-center_size+holder_radius, 0, center_height]){
    translate([0,-hole_distance2,0]){
      holeh(hole_radius, center_height*2);
    }
    
    translate([0,hole_distance2,0]){
      holeh(hole_radius, center_height*2);
    }
  }
  }

module base(){
  difference(){
    union(){
      //base
      base_raw(center_height);
      
      //arm mount
      for(i = [0 : arm_angle : 359])
        rotate([0,0,i])translate([-center_size+holder_radius, 0, center_height - holder_height])arm_holder();
    }
    
    //control board
    translate([-board_width/2, -board_length/2+20, center_height - center_hole_depth]){
      cube(size = [board_width,board_length,board_height]);
    }
    translate([0, 20, -center_height/2])cylinder(r = cable_hole_radius, h = center_height);
  
    //battery mount 1
    translate([-board_width,0,0])batery_mount();
    
    //battery mount 2
    translate([board_width,0,0])batery_mount();
    
    //receiver
    translate([-receiver_length/2, -board_length+20, center_height - center_hole_depth]){
      cube(size = [receiver_length,receiver_width,receiver_height]);
    }
    translate([0, -board_length + receiver_width/2+20, -center_height/2])cylinder(r = cable_hole_radius, h = center_height*2);
    
    //mount holes
    for(i = [0 : arm_angle : 359])
      rotate([0,0,i])mount_holes();
    
    //leg mount
    for(i = [0 : arm_angle : 359]) rotate([180,0,i])translate([-center_size+holder_radius, 0, -center_hole_depth]) 
    {
      //rotate([0,0,180])leg();
      holder(holder_radius);
      translate([-20, -arm_width/2, 0])cube([20,arm_width,arm_height+1]);
    }
    
    //cable mount holes
    for(i = [1 : num_of_arms]) {
      rotate([0,0,i*arm_angle + arm_angle/3])translate([center_size - cable_mount_hole_radius * 2, 0, -center_height/2])cylinder(r=cable_mount_hole_radius, h=center_height*2);
      rotate([0,0,i*arm_angle + arm_angle/3*2])translate([center_size - cable_mount_hole_radius * 2, 0, -center_height/2])cylinder(r=cable_mount_hole_radius, h=center_height*2);
      rotate([0,0,i*arm_angle + arm_angle/2])translate([center_size - cable_mount_hole_radius * 2, 0, -center_height/2])cylinder(r=cable_mount_hole_radius, h=center_height*2);
      rotate([0,0,i*arm_angle + arm_angle/2])translate([center_size - cable_hole_radius * 3, 0, -center_height/2])cylinder(r=cable_hole_radius, h=center_height*2);
    }
  }
}

module batery_mount(){
  translate([-battery_width/2-battery_strap_height/2, -battery_strap_width/2, -center_height/2])cube([battery_strap_height,battery_strap_width, center_height*2]);
  translate([battery_width/2-battery_strap_height/2, -battery_strap_width/2, -center_height/2])cube([battery_strap_height,battery_strap_width, center_height*2]);
}

base();
