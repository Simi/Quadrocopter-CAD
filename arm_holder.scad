include <constants.scad>;
use <arm_v2.scad>;
use <modules.scad>;


module arm_holder(){
  intersection(){
    difference(){
      cylinder(r=mount_radius, h=arm_height/2 + holder_height);

      translate([0, 0, holder_height]) holder(holder_radius);
      translate([-20, -arm_width/2, holder_height])cube([20,arm_width,arm_height+1]);

      //Mount Holes
      translate([0,-hole_distance2,-arm_height/2]){
        hole(hole_radius);
      }

      translate([0,hole_distance2,-arm_height/2]){
        hole(hole_radius);
      }
    }

  translate([center_size-holder_radius, 0, -center_height*2]) base_raw(center_height*5); 
  }
}

arm_holder();