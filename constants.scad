num_of_arms = 6;
proppler_size = 10; //inches
arm_angle = 360/num_of_arms;

def_fn = 30;
$fn = def_fn;

arm_width = 24;
arm_height = 15;

battery_width = 35;
battery_length = 110;
battery_strap_width = 25;
battery_strap_height = 5;

board_width = 45;
board_length = 71;
board_height = 14;

cable_mount_hole_radius = 3;
cable_hole_radius = 8;
cable_hole_size = 5;

center_size = 100;
center_height = 8;
center_hole_depth = 5;

electonic_height = 75;

gps_size = 56;
gps_inner = 20;

hole_radius = 2.5;
hole_distance = 10;
hole_distance1 = 12.5;
hole_distance2 = 9.5;

leg_width = 12;
leg_length = 150;
leg_height = 12;

holder_radius = 15;
holder_height = 3;

motor_radius = 18;
motor_screw_radius = 1.5;
motor_height = arm_height;
motor_hole_radius = 5.5;
mount_radius = holder_radius + 5;

receiver_height = 15;
receiver_width = 26;
receiver_length_without_cables = 56;
receiver_length = receiver_length_without_cables + 20;

side_thickness = 4;

top_height = 10;

//propler size * 2.54 (convert to cm) * 10 (convert to mm)
// * 2 (2 proplers are in triangle) 
// / 2 (only half of propler in triangle)
// + 40 (spacing between propplers)
// - center size (remove center from triangle)
arm_length = proppler_size * 2.54 * 10 * 2 / 2 + 40 - center_size;
arm_shift = arm_length/2; //sqrt(hole_distance*hole_distance)/2 + 2*hole_radius+1;
